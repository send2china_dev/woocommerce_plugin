<?php
/**
 * Plugin Name: Your Shipping plugin
 * Plugin URI:        https://sms.send2china.com
 * Description:       Integration Woocommerce with Send2China BC
 * Version:           0.1 
 * Requires at least: 5.2
 * Requires PHP:      7.2
 * Author:            Tim Chou 
 * License:           GPL v2 or later
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
*/


if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {

    // Add a custom dynamic BC tax fee
    add_action( 'woocommerce_cart_calculate_fees', 'add_bc_tax', 20, 1 );
    function add_bc_tax( $cart ) {
        if ( is_admin() && ! defined( 'DOING_AJAX' ) )
            return;

        $domain      = "woocommerce";
        $tax = WC()->session->get( '_s2c_bc_tax');

        $cost  = $tax;
        $label = __("BC tax", $cost);

        if ( isset($cost) )
            $cart->add_fee( $label, $cost );
    }


    function cal_tax_and_freight_by_s2c_api( $package){
        /*
         */
                        
        $contents = array();
        foreach ( $package['contents'] as $item_id => $values ){
            $_product = $values['data'];
                        //print "<pre>";
                        //print_r($values);
                        //print "</pre>";
            
            $contents[] = array(
                "sku"       => $_product->get_sku(),
                "price"     => $values['line_subtotal'] / $values['quantity'],
                "quantity"  => $values['quantity'],
            );
        }


        $url = 'https://send2china.co.uk/api/core/express_and_tax_calculate/';
        if("http://127.0.0.1:8887" == get_bloginfo("url")){
            $url = 'http://www.s2clocal.co.uk/api/core/express_and_tax_calculate/';
        }

        $token = get_option( 's2c_token' );
        $headers = array(
            'Authorization' => 'token '.$token ,
            'Content-Type' => 'application/json'
        );


        $body = array(
            "currency" => get_woocommerce_currency(),
            "items" => $contents
        );
        //var_dump($body);
        //die(12121);

        // post to the request somehow
        $r = wp_remote_post( $url, array(
            'method'        => 'POST',
            'timeout'       => 45,
            'redirection'   => 5,
            'httpversion'   => '1.0',
            'blocking'      => true,
            'headers'       => $headers,
            'body'          => json_encode($body)
        ));
        //print_r($r);
        //die("products_first_ends");

        if ( is_wp_error( $r) ) {
            //$error_message = $r->get_error_message();
            //var_dump( "Something went wrong: $error_message");
            return array(
                "result" => false
            );
        } else {
            $result = json_decode($r['body']);
            if($result->code == 0){
                //success
                return array(
                    "result" => true,
                    "tax"    => $result->data[0]->tax + $result->data[0]->express_tax,
                    "freight"=> $result->data[0]->express_fee,
                );

            }else{
                return array(
                    "result" => false
                );
            }
            //die("exit");
        }

    }

    function your_shipping_method_init() {
        if ( ! class_exists( 'WC_Your_Shipping_Method' ) ) {
            class WC_Your_Shipping_Method extends WC_Shipping_Method {
                /**
                 * Constructor for your shipping class
                 *
                 * @access public
                 * @return void
                 */
                public function __construct() {
                    $this->id                 = "s2cddp"; // Id for your shipping method. Should be uunique.
                    $this->method_title       = "DDP China";  // Title shown in admin
                    $this->method_description = "Duty Paid Service to China (limited to a single item at above 5000RMB)"; // Description shown in admin

                    $this->title              = isset( $this->settings['title'] ) ? $this->settings['title'] : 'DDP China'; 
                    $this->enabled            = isset( $this->settings['enabled'] ) ? $this->settings['enabled'] : 'yes';

                    $this->availability       = 'including';
                    $this->countries          = array('CN',);

                    $this->init();
                }

                /**
                 * Init your settings
                 *
                 * @access public
                 * @return void
                 */
                function init() {
                    // Load the settings API
                    $this->init_form_fields(); // This is part of the settings API. Override the method to add your own settings
                    $this->init_settings(); // This is part of the settings API. Loads settings you previously init.

                    // Save settings in admin if you have any defined
                    add_action( 'woocommerce_update_options_shipping_' . $this->id, array( $this, 'process_admin_options' ) );
                }


                function init_form_fields() {

                    $this->form_fields = array(

                     'enabled' => array(
                          'title' => 'Enable',
                          'type' => 'checkbox',
                          'description' => 'Enable this shipping.',
                          'default' => 'yes'
                          ),

                     'taxable' => array(
                          'title' => 'Enable tax and freight',
                          'type' => 'checkbox',
                          'description' => 'Calculate tax and express fee',
                          'default' => 'yes'
                          ),

                     'title' => array(
                        'title' => 'Title',
                          'type' => 'text',
                          'description' => 'Title to be display on site',
                          'default' => 'DDP China'
                          ),


                     );

                }


                /**
                 * calculate_shipping function.
                 *
                 * @access public
                 * @param mixed $package
                 * @return void
                 */
                public function calculate_shipping( $package =  array()) {
                    $is_api = isset( $this->settings['taxable'] ) && $this->settings['taxable'] == 'no' ? false : true;
                    if($is_api){
                        $r = cal_tax_and_freight_by_s2c_api($package);
                        if($r['result']){
                            
                           WC()->session->set( '_s2c_bc_tax',$r['tax']);

                           $rate = array(
                               //tax and freight will togather
                               'id' => $this->id,
                               'label' => $this->title,
                               //'cost' =>  $r['freight'] + $r['tax'],
                               'cost' => $r['freight'],
                               
                               /*'taxes'=> array(
                                   "bc" => $r['tax']
                               
                               ),*/
                               'calc_tax' => 'per_order'
                           );
                           $this->add_rate( $rate );
                        }
                    }else{
                        $rate = array(
                                'id' => $this->id,
                                'label' => $this->title,
                                'cost' => '0',
                                'calc_tax' => 'per_item'
                        );
                        $this->add_rate( $rate );
                    }
                }
            }
        }
    }

    add_action( 'woocommerce_shipping_init', 'your_shipping_method_init' );

    function add_your_shipping_method( $methods ) {
            $methods[] = 'WC_Your_Shipping_Method';
            return $methods;
    }

    add_filter( 'woocommerce_shipping_methods', 'add_your_shipping_method' );
} 
?>
