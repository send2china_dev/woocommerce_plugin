<?php
/**
 * Plugin Name:       Send2China BC 
 * Plugin URI:        https://sms.send2china.com
 * Description:       Integration Woocommerce with Send2China BC
 * Version:           0.1 
 * Requires at least: 5.2
 * Requires PHP:      7.2
 * Author:            Tim Chou 
 * License:           GPL v2 or later
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
*/


if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
    
    // custom check province\city\county fields
    add_action( 'wp_footer', 'change_checkout_fields_script' );
    function change_checkout_fields_script() {
        // Only checkout page
        if( is_checkout() && ! is_wc_endpoint_url() ):
        ?>
            <script src="https://send2china.co.uk/site_media/static/JAreaSelect/JAreaData.js"></script>
            <script src="https://send2china.co.uk/site_media/static/JAreaSelect/JAreaSelect.js"></script>
            <script src="https://send2china.co.uk/site_media/static/JAreaSelect/JAreaSelect2.js"></script>
            <script type="text/javascript">
                jQuery(function($){
                    var d = $("#billing_addr_field");
                    if ( $( "#buyer_addr_field" ).length ) {
                        d = $("#buyer_addr_field");
                    }

                    d.empty();
                    
                    d.append(
                        '<label for="buyer_addr" class="">省市区&nbsp;<abbr class="required" title="required">*</abbr></label>'
                    );
                    d.append(
                        '<input type="text" name="buyer_addr" id="buyer_addr" style="display:none;">'
                    );
                    d.append(
                        '<div id="p-c-c"></div>'
                    );

                    var area = $("#p-c-c").JAreaSelect({
                        selectClassName: "input-text",
                    });

                    function get_and_set_selected_area(){
                        var province = $( 'select[name ="province"] option:selected' ).text();
                        var city = $( 'select[name ="city"] option:selected' ).text();
                        var county = $( 'select[name ="dist"] option:selected' ).text();
                        console.log(province, city, county);
                        $("#buyer_addr").val(province + "#" + city + "#" + county);
                    }

                    $(document).on('change', 'select[name ="province"]', function() {
                        get_and_set_selected_area();
                    });

                    $(document).on('change', 'select[name ="city"]', function() {
                        get_and_set_selected_area();
                    });

                    $(document).on('change', 'select[name ="dist"]', function() {
                        get_and_set_selected_area();
                    });

                });

            </script>
        <?php
        endif;
    };


    add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );
    function custom_override_checkout_fields( $fields ) {
        /*
            1. add custom fields:
                order_name: Payer's Chinese Name
                order_cid:  Payer's Chinese ID Number

            2. display custom fields when customer select BC shipping method
         */
        
        $bc_shipping_method ='s2cddp';
        global $woocommerce;
        $chosen_methods = WC()->session->get( 'chosen_shipping_methods' );
        $chosen_shipping = $chosen_methods[0];

        if ($chosen_shipping == $bc_shipping_method) {
            //unset($fields['billing']['billing_address_1']); // Add/change filed name to be hide
            unset($fields['billing']['billing_last_name']);
            unset($fields['billing']['billing_company']);
            unset($fields['billing']['billing_address_2']);

            $fields['billing']['billing_first_name']['priority'] = 1;
            $fields['billing']['billing_phone']['priority'] = 5;
            $fields['billing']['billing_email']['priority'] = 10;
            $fields['billing']['billing_country']['priority'] = 15;
            $fields['billing']['billing_address_1']['priority'] = 30;
            $fields['billing']['billing_postcode']['priority'] = 35;
            
            $fields['billing']['billing_first_name']['label'] = "收件人姓名";
            $fields['billing']['billing_first_name']['placeholder'] = "请务必填写真实的中文收件人姓名，否则无法清关";

            $fields['billing']['billing_phone']['label'] = "手机";
            $fields['billing']['billing_country']['label'] = "国家";
            

            $fields['billing']['billing_address_1']['label'] = "详细地址";
            $fields['billing']['billing_postcode']['label'] = "邮编";


            $fields['billing']['buyer_addr'] = array(
                'label' => __('省市区', 'woocommerce'),
                'placeholder' => _x('省市区', 'placeholder', 'woocommerce'),
                'class' => array('form-row-wide'),
                'required' => true,
                'clear' => true,
                'priority' => 26,
            );
            $fields['billing']['order_name'] = array(
                'label' => __('付款人中文姓名', 'woocommerce'),
                'placeholder' => _x('请填写付款人真实姓名(清关用)', 'placeholder', 'woocommerce'),
                'required' => true,
                'class' => array('form-row-wide'),
                'clear' => true,
                'priority' => 40,
            );
            $fields['billing']['order_cid'] = array(
                'label' => __('付款人身份证号', 'woocommerce'),
                'placeholder' => _x('请填写付款人身份证号码(清关用)', 'placeholder', 'woocommerce'),
                'required' => true,
                'class' => array('form-row-wide'),
                'clear' => true,
                'priority' => 45,
            );
            
            unset($fields['billing']['billing_state']);
            unset($fields['billing']['billing_city']);
        }

        return $fields;
    }

    add_filter( 'woocommerce_default_address_fields' , 'custom_override_default_address_fields' );
    function custom_override_default_address_fields( $address_fields ) {
        $bc_shipping_method ='s2cddp';
        global $woocommerce;
        $chosen_methods = WC()->session->get( 'chosen_shipping_methods' );
        $chosen_shipping = $chosen_methods[0];

        if ($chosen_shipping == $bc_shipping_method) {
            $address_fields['address_1']['priority'] = 30;
            $address_fields['postcode']['priority'] = 35;
            
            $address_fields['address_1']['label'] = "详细地址";
            $address_fields['postcode']['label'] = "邮编";

            unset($address_fields['state']);
            unset($address_fields['city']);

        }
        return $address_fields;
    }
    
    add_action('woocommerce_checkout_process', 'checkout_fields_validation');
    function checkout_fields_validation() {
        /*
            array(25) {
  ["billing_first_name"]=>
  string(6) "MINJUN"
  ["billing_phone"]=>
  string(11) "18013699969"
  ["billing_email"]=>
  string(17) "tim0405@gmail.com"
  ["billing_country"]=>
  string(2) "CN"
  ["buyer_addr"]=>
  string(26) "湖南#湘潭市#雨湖区"
  ["province"]=>
  string(2) "18"
  ["city"]=>
  string(4) "1495"
  ["dist"]=>
  string(5) "29448"
  ["billing_address_1"]=>
  string(18) "9 Jefferson Street"
  ["billing_postcode"]=>
  string(5) "02141"
  ["order_name"]=>
  string(3) "zmj"
  ["order_cid"]=>
  string(18) "230103196704217625"
  ["shipping_first_name"]=>
  string(6) "MINJUN"
  ["shipping_last_name"]=>
  string(0) ""
  ["shipping_phone"]=>
  string(11) "18013699969"
  ["shipping_company"]=>
  string(0) ""
  ["shipping_address_1"]=>
  string(18) "9 Jefferson Street"
  ["shipping_postcode"]=>
  string(5) "02141"
  ["shipping_country"]=>
  string(2) "CN"
  ["shipping_address_2"]=>
  string(0) ""
  ["order_comments"]=>
  string(0) ""
  ["shipping_method"]=>
  array(1) {
    [0]=>
    string(6) "s2cddp"
  }
  ["payment_method"]=>
  string(4) "bacs"
  ["woocommerce-process-checkout-nonce"]=>
  string(10) "d7372c8144"
  ["_wp_http_referer"]=>
  string(29) "/?wc-ajax=update_order_review"
}
         */
        $name = $_POST['billing_first_name'];
        $phone = $_POST['billing_phone'];
        $buyer_addr = $_POST['buyer_addr'];
        $billing_address_1 = $_POST['billing_address_1'];
        $billing_postcode = $_POST['billing_postcode'];
        $order_name = $_POST['order_name'];
        $order_cid = $_POST['order_cid'];
        $shipping_method = $_POST['shipping_method'];
        $payment_method = $_POST['payment_method'];

        if(empty($name)){
            wc_add_notice( '收件人姓名是必填项', 'error' );
        }
        if(!preg_match('/^[\x{4e00}-\x{9fa5}]+$/u', $name)){
            wc_add_notice( '请填写正确的收件人姓名', 'error' );
        }

        if(empty($phone)){
            wc_add_notice( '收件人手机号是必填项', 'error' );
        }
        if(!preg_match("/^1[34578]\d{9}$/", $phone)){
            wc_add_notice( '请填写正确的收件人手机号', 'error' );
        }

        $addr = explode("#",$buyer_addr);
        if(count($addr) != 3){
            wc_add_notice( '请完整选择省市区', 'error' );
        }
        $province = $addr[0];
        $city = $addr[1];
        $county = $addr[2];

        if(empty($billing_address_1)){
            wc_add_notice( '详细地址是必填项', 'error' );
        }

        if(empty($billing_postcode)){
            wc_add_notice( '邮编是必填项', 'error' );
        }

        if(empty($order_name)){
            wc_add_notice( '付款人姓名是必填项', 'error' );
        }
        if(!preg_match('/^[\x{4e00}-\x{9fa5}]+$/u', $order_name)){
            wc_add_notice( '请填写正确的付款人姓名', 'error' );
        }

        if(empty($order_cid)){
            wc_add_notice( '付款人身份证号是必填项', 'error' );
        }
        if(strlen($order_cid) != 18){
            wc_add_notice( '付款人身份证号格式有误', 'error' );
        }

        if(count($shipping_method) == 0){
            wc_add_notice( '请选择快递方式', 'error' );
        }
        $shipping_method = $shipping_method[0];
        if($shipping_method != "s2cddp"){
            wc_add_notice( '请选择BC快递方式', 'error' );
        }
     

        /*
            if ( empty( $_POST['contactmethod'] ) )
         */
     
    }

    add_action( 'woocommerce_checkout_update_order_meta', 'misha_save_what_we_added' );
    function misha_save_what_we_added( $order_id ){
        $buyer_addr = $_POST['buyer_addr'];
        $order_name = $_POST['order_name'];
        $order_cid = $_POST['order_cid'];
        
        $addr = explode("#",$buyer_addr);
        $province = $addr[0];
        $city = $addr[1];
        $county = $addr[2];

        $tax = WC()->session->get( '_s2c_bc_tax');
        update_post_meta( $order_id, 'tax', sanitize_text_field($tax) );
        
        update_post_meta( $order_id, 'province', sanitize_text_field($province) );
        update_post_meta( $order_id, 'city', sanitize_text_field($city) );
        update_post_meta( $order_id, 'county', sanitize_text_field($county) );
        update_post_meta( $order_id, 'order_name', sanitize_text_field( $_POST['order_name'] ) );
        update_post_meta( $order_id, 'order_cid', sanitize_text_field( $_POST['order_cid'] ) );
     
    }

    add_action( 'woocommerce_admin_order_data_after_billing_address', 'my_custom_checkout_field_display_admin_order_meta', 10, 1 );
    function my_custom_checkout_field_display_admin_order_meta($order){
        //echo "<pre>";
        //print_r($order->get_items('fee'));
        //echo "</pre>";

        echo '<p><strong>'.__('省').':</strong> ' . get_post_meta( $order->get_id(), 'province', true ) . '</p>';
        echo '<p><strong>'.__('市').':</strong> ' . get_post_meta( $order->get_id(), 'city', true ) . '</p>';
        echo '<p><strong>'.__('区').':</strong> ' . get_post_meta( $order->get_id(), 'county', true ) . '</p>';
        echo '<p><strong>'.__('付款人中文姓名').':</strong> ' . get_post_meta( $order->get_id(), 'order_name', true ) . '</p>';
        echo '<p><strong>'.__('付款人身份证号').':</strong> ' . get_post_meta( $order->get_id(), 'order_cid', true ) . '</p>';
    }

} 
?>
