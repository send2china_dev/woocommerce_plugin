<?php
/**
 * Plugin Name:       Send2China BC 
 * Plugin URI:        https://sms.send2china.com
 * Description:       Integration Woocommerce with Send2China BC
 * Version:           0.1 
 * Requires at least: 5.2
 * Requires PHP:      7.2
 * Author:            Tim Chou 
 * License:           GPL v2 or later
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
*/


if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
    //$token = get_option( 's2c_token' );

    // cron start
    /*
    add_filter( 'cron_schedules', 'check_bc_orders' );
    function check_bc_orders( $schedules ) {
        $schedules['five_seconds'] = array(
            'interval' => 300,
            'display'  => esc_html__( 'Every 300 Seconds' ), );
        return $schedules;
    }

    if ( ! wp_next_scheduled( 'bl_cron_hook' ) ) {
        wp_schedule_event( time(), 'five_seconds', 'check_bc_order' );
    }
    
    add_action( 'check_bc_order', 'check_all_bc_orders' );
    function check_all_bc_orders(){
        $orders = wc_get_orders( array(
            'status' => 'completed',
        ) );

        foreach ($orders as $order) {
            error_log($order);
        }
    }*/
    // cron end 
    
    // start 
    // admin order list page ,add custom column
    add_filter( 'manage_edit-shop_order_columns', 'bbloomer_add_new_order_admin_list_column' );
    function bbloomer_add_new_order_admin_list_column( $columns ) {
        $columns['s2c_api_data'] = 'S2C API Data';
        return $columns;
    }

    add_action( 'manage_shop_order_posts_custom_column', 'bbloomer_add_new_order_admin_list_column_content' );
    function bbloomer_add_new_order_admin_list_column_content( $column ) {

        global $post;

        if ( 's2c_api_data' === $column ) {

            $order = wc_get_order( $post->ID );
            $sd_num= $order->get_meta('_s2c_api_sd_number');
            $label = $order->get_meta('_s2c_api_label');
            $err_msg = $order->get_meta('_s2c_api_msg');
            if($sd_num){
                echo '<p>SD Number: <span class="text-primary">'.$sd_num.'<span></p>';
            }
            if($label){
                echo '<p><a href="'.$label.'" target="_blank">Label </a></p>';
            }
            if($err_msg){
                echo '<p>Log: '.$err_msg.'</p>';
            }

        }
    }
    // end 

    // start
    // custom admin action: push order to send2china bc api
    function sv_wc_add_order_push_bc_order( $actions ) {
        global $theorder;

        // bail if the order has been paid for or this action has been run
        //if ( ! $theorder->is_paid() || get_post_meta( $theorder->id, '_is_bc') != "1") {
        if ( ! $theorder->is_paid() ) {
            return $actions;
        }

        $actions['wc_custom_order_action_push_order'] = 'Push order to S2C';
        return $actions;
    }
    add_action( 'woocommerce_order_actions', 'sv_wc_add_order_push_bc_order' );

    function sv_wc_push_order_to_sms( $order ) {
        // do fetch
        push_bc_order($order->get_id());

        // add the order note
        // translators: Placeholders: %s is a user's display name
        $message = sprintf( __( 'Push BC order to Send2China SMS API by %s.', 'my-textdomain' ), wp_get_current_user()->display_name );
        $order->add_order_note( $message );

    }
    add_action( 'woocommerce_order_action_wc_custom_order_action_push_order', 'sv_wc_push_order_to_sms' );
    // end

    // start
    // custom action: fetch label
    function sv_wc_add_order_fetch_label( $actions ) {
        global $theorder;

        // bail if the order has been paid for or this action has been run
        //if ( ! $theorder->is_paid() || get_post_meta( $theorder->id, '_is_bc') != "1") {
        if ( ! $theorder->is_paid() ) {
            return $actions;
        }

        // add "mark printed" custom action
        $actions['wc_custom_order_action_fetch_label'] = __( 'Fetch BC Label', 'my-textdomain' );
        return $actions;
    }
    add_action( 'woocommerce_order_actions', 'sv_wc_add_order_fetch_label' );

    function sv_wc_process_order_fetch_label( $order ) {
        // do fetch
        fetch_bc_label($order->get_id());

        // add the order note
        // translators: Placeholders: %s is a user's display name
        $message = sprintf( __( 'Fetch BC Label by %s.', 'my-textdomain' ), wp_get_current_user()->display_name );
        $order->add_order_note( $message );

    }
    add_action( 'woocommerce_order_action_wc_custom_order_action_fetch_label', 'sv_wc_process_order_fetch_label' );
    // end
    
    // start
    // called when order status change
    function after_order_complete( $order_id, $old_status, $new_status ) {
        
        //error_log(  $order_id );
        //error_log(  $old_status);
        //error_log(  $new_status);
            
        $order = wc_get_order( $order_id );
        //error_log( $order);

        if ( $new_status == "completed" ) {
            //delete_post_meta( $order_id, 'customer_username' );
            //delete_post_meta( $order_id, 'customer_password' );
            //delete_post_meta( $order_id, 'customer_website' );
            push_bc_order($order_id);
        }
    }
    add_action( 'woocommerce_order_status_changed', 'after_order_complete', 10, 3 );
    // end 

    // start
    // admin order detail page; metadata editable
    add_action( 'woocommerce_admin_order_data_after_shipping_address', 'misha_editable_order_meta_general' );
    function misha_editable_order_meta_general( $order ){  
        $sd_num = get_post_meta( $order->get_id(), '_s2c_api_sd_number', true);
        $label = get_post_meta( $order->get_id(), '_s2c_api_label', true);
        $msg = get_post_meta( $order->get_id(), '_s2c_api_msg', true);

        echo '<br class="clear" />';
        echo '<h4>Send2China BC status</h4>';

        echo '<div class="address">';
        if(!$sd_num){
            echo "-";
        }
        woocommerce_wp_text_input( array(
            'id' => 'sd_num',
            'label' => 'SD Number:',
            'value' => $sd_num,
            'wrapper_class' => 'form-field-wide',
        ) );

        woocommerce_wp_text_input( array(
            'id' => 'label',
            'label' => 'Label:',
            'value' => $label,
            'wrapper_class' => 'form-field-wide'
        ) );
        if($label){
            echo '<a href='.$label.' target="_blank">Download</a>';
        }
        
        woocommerce_wp_textarea_input( array(
            'id' => 'log',
            'label' => 'Log:',
            'value' => $msg,
            'wrapper_class' => 'form-field-wide'
        ) );

		echo '</div>';
    }

    add_action( 'woocommerce_process_shop_order_meta', 'misha_save_general_details' );

    function misha_save_general_details( $ord_id ){
        update_post_meta( $ord_id, '_s2c_api_sd_number', wc_clean( $_POST[ 'sd_num' ] ) );
        update_post_meta( $ord_id, '_s2c_api_label', wc_clean( $_POST[ 'label' ] ) );
        update_post_meta( $ord_id, '_s2c_api_msg', wc_clean( $_POST[ 'log' ] ) );
    }
    // end 


    function fetch_bc_label( $order_id ){
        /*
            after we push order to send2china, label will be generated async,
            we need to call this function to get label
         */

        $order = wc_get_order( $order_id );

        $url = 'https://send2china.co.uk/api/v2/shipment/?order_id='.$order_id;
        
        if("http://127.0.0.1:8887" == get_bloginfo("url")){
            // local dev
            $url = 'http://www.s2clocal.co.uk/api/v2/shipment/?order_id='.$order_id;
        }

        $token = get_option( 's2c_token' );
        $headers = array(
            'Authorization' => 'token '.$token ,
            'Content-Type' => 'application/json'
        );

        //var_dump($body);
        //die(12121);

        // post to the request somehow
        $r = wp_remote_get( $url, array(
            'headers'       => $headers, 
        ));
        $body = wp_remote_retrieve_body( $r);
        //print_r($body);
        //die("products_first_ends");

        $result = json_decode($body);
        if($result->code == 0){

            if($result->data->result == true){
                //success
                $order->update_meta_data( '_s2c_api_sd_number', $result->data->sd_number );
                $order->update_meta_data( '_s2c_api_label', $result->data->label_url);
                $order->update_meta_data( '_s2c_api_msg', "Fetch label successfully.");
                var_dump("success");
            }else{
                $order->update_meta_data( '_s2c_api_msg', $result->data->log);
                var_dump("return log error:", $result->data->log);
            }
        }else{
            $order->update_meta_data( '_s2c_api_msg', $result->desc );
            var_dump("SMS API return error:", $result->desc);
        }
        $order->save();
        //die("exit");

    } 

    function push_bc_order( $order_id ){
        /*
            once we change order status to completed,this function will be called,
            will send order to send2china bc api.If there were errors,we will store 
            error message in metadata
         */

        $order = wc_get_order( $order_id );
        error_log( $order);

        $contents = array();
        foreach ($order->get_items() as $item) {
            $product = wc_get_product($item->get_product_id());
            $contents[] = array(
                "SKU"           => $product->get_sku(),
                "ProductName"   => $product->get_title(),
                "ProductPrice"  => $item['subtotal'] /  $item['quantity'],
                "Quantity"      => $item['quantity'],
            );
        }

        $url = 'https://send2china.co.uk/api/v2/order/';
        if("http://127.0.0.1:8887" == get_bloginfo("url")){
            $url = 'http://www.s2clocal.co.uk/api/v2/order/';
        }

        $token = get_option( 's2c_token' );
        $headers = array(
            'Authorization' => 'token '.$token ,
            'Content-Type' => 'application/json'
        );



        // Get order total
        $order_total = is_callable(array($order, 'get_total')) ? $order->get_total() : $order->order_total;
        $order_total = floatval($order_total);

        // Get order subtotal
        $order_subtotal = floatval($order->get_subtotal());

        // Get order total discount
        $order_discount_total = floatval($order->get_discount_total());

        $shipping_total = floatval($order->get_shipping_total());
        $tax_total = get_post_meta( $order->get_id(), 'tax', true );
        /*
        foreach( $order->get_items('fee') as $item_id => $item_fee ){
            $fee_name = $item_fee->get_name();
            if($fee_name == "BC tax"){
                $tax_total = $item_fee->get_total();
            }
        }
        //$tax_total += $shipping_total * 0.091; 
        $tax_total = round($tax_total , 2);
         */
        
        $order_name = $order->get_meta('order_name');
        $order_cid = $order->get_meta('order_cid');
        $province = $order->get_meta('province');
        $city = $order->get_meta('city');
        $county = $order->get_meta('county');

        //$payment_method = $order->get_payment_method();
        //$payment_tid = $order->get_transaction_id();
        $payment_method = "2";
        $payment_tid = "99q377997";

        $phone = $order->get_billing_phone();
        //var_dump($payment_method, $payment_tid, $phone);
        //die(12121);

        
        $body = array( 
            'source'                    => 'woocommerce',
            'MerchantName'              => get_bloginfo("name"), 
            'SalesChannel'              => 0, 
            'SalesWebAddress'           => get_bloginfo("url"), 
            //'OrderCarrierServiceCode'   => 'ddp', 
            'OrderCarrierServiceCode'   => 'DDP3', 
            'OrderID'                   => $order_id, 
            'Currency'                  => 'RMB', 
            'PayableAmount'             => $order_total, 
            'ProductAmount'             => $order_subtotal, 
            'CustomTaxAmount'           => $tax_total, 
            'ShippingAmount'            => $shipping_total, 
            'DiscountAmount'            => $order_discount_total,
            'OrderPayerName'            => $order_name, 
            'OrderPayerIDNumber'        => $order_cid, 
            'OrderPayerMobile'          => $phone, 
            'PaymentMethod'             => $payment_method, 
            "PaymentTransactionID"      => $payment_tid,
            "PaymentTime"               => $order->get_date_paid()->getTimestamp(),
            "ConsigneeName"             => $order->get_shipping_first_name(), //$order->get_shipping_last_name().$order->get_shipping_first_name(),
            "ConsigneePhone"            => $phone,
            "ConsigneeAddress1"         => $order->get_shipping_address_1()." ".$order->get_shipping_address_2(),
            "ConsigneeCity"             => $city, //$order->get_shipping_city(),
            "ConsigneeDistrict"         => $county, //$order->get_shipping_city(),
            "ConsigneeState"            => $province, //trim(explode('/', WC()->countries->get_states( 'CN' )['CN11'])[1]),
            "ConsigneeCountry"          => "中国",
            "ConsigneePostcode"         => $order->get_shipping_postcode(),
            "contents"                  => $contents
        );
        //var_dump($body);
        //die(12121);

        // post to the request somehow
        $r = wp_remote_post( $url, array(
            'method'        => 'POST',
            'timeout'       => 45,
            'redirection'   => 5,
            'httpversion'   => '1.0',
            'blocking'      => true,
            'headers'       => $headers, 
            'body'          => json_encode($body)
        ));
        //print_r($r);
        //die("products_first_ends");
        
        $order->update_meta_data( '_is_bc', '1' );
        $order->save();

        if ( is_wp_error( $r) ) {
            $error_message = $r->get_error_message();
            var_dump( "Something went wrong: $error_message");
            die("exit");
        } else {
            $result = json_decode($r['body']);
            if($result->code == 0){
                //success
                //$order->update_meta_data( '_s2c_api_sd_number', 'SD777777GB' );
                //wc_delete_order_item_meta( $order_id, "_s2c_api_msg");
                
                for ($i=0; $i <= 5; $i++){
                    fetch_bc_label($order->get_id());

                    $sd_num= $order->get_meta('_s2c_api_sd_number');
                    if($sd_num){
                        break;
                    }

                    sleep(1);
                }

                $sd_num= $order->get_meta('_s2c_api_sd_number');
                if(!$sd_num){
                    $order->update_meta_data( '_s2c_api_msg', "Push order to S2C successfullt,waiting for label generate.");
                }

                var_dump("success");
            }else{
                var_dump("SMS API return error:", $result->desc);
                $order->update_meta_data( '_s2c_api_msg', $result->desc );
            }
            $order->save();
            //die("exit");
        }

    } 




}
?>
