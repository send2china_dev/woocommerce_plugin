<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://sms.send2china.com
 * @since             1.0.0
 * @package           Send2China 
 *
 * @wordpress-plugin
 * Plugin Name:       Send2China BC 
 * Plugin URI:        https://sms.send2china.com
 * Description:       Integration with Send2China BC Service
 * Version:           1.0.0
 * Author:            Minjun Zhou 
 * Author URI:        https://sms.send2china.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       plugin-send2china
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'PLUGIN_NAME_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-plugin-send2china-activator.php
 */
function activate_plugin_send2china() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-plugin-send2china-activator.php';
	Plugin_Send2china_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-plugin-send2china-deactivator.php
 */
function deactivate_plugin_send2china() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-plugin-send2china-deactivator.php';
	Plugin_Send2china_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_plugin_send2china' );
register_deactivation_hook( __FILE__, 'deactivate_plugin_send2china' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-plugin-send2china.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_plugin_send2china() {

	$plugin = new Plugin_Send2china();
	$plugin->run();

}
run_plugin_send2china();
